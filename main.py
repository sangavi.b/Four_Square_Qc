import pandas as pd

def compare_foursquare_id(column):     
    if column["foursquare_id"] == column["FsqID"]:
        return 1
    elif (pd.isnull (column["foursquare_id"]) & pd.isnull(column["FsqID"])):
        return 2
    elif pd.isnull(column["foursquare_id"]):
        return 3
    elif pd.isnull(column["FsqID"]):
        return 4
    elif column["foursquare_id"] != column["FsqID"]:
        return 5